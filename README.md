# FLASK PYTEST Docker and GITLAB CI


## Installation

---

```shell
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```
# Execution all the Tests

To execute all the tests, run the following command:

```bash
pytest
```

- Execution all the Tests with stdout response

To execute all the tests, run the following command:

```bash
pytest -s
```

- Executing only grouped Tests

To only execute the `get_request` grouped tests, run the following command:

```bash
pytest -m get_request
```

# gitlab - CI

1. disable or enable stages on the gilab Ci

```yaml
deploy:
  stage: deploy
  when: manual
```

2. You could disable all the jobs from your stage using this trick of starting the job name with a dot ('.')

```yaml
.hidden_job:
  script:
    - run test
```

# GITLAB RUNNER

- use docker executor , install docker on vm(ec2)
- add ec2-user/ubuntu to the docker grop
```sh 
usermod -aG docker ubuntu

root@ubuntu:/home/ubuntu# grep docker /etc/group
docker:x:122:ubuntu
```

## installation

https://docs.gitlab.com/runner/install/linux-repository.html

## register gitlab runner
```
gitlab-runner register
```
> select - docker as executor 

## check the status of gitlab-runner

```
root@ubuntu:/home/ubuntu# gitlab-runner status
Runtime platform                                    arch=amd64 os=linux pid=2304 revision=4d1ca121 version=15.8.2
gitlab-runner: Service is running
```

## group runners on group level
> default runs with no tags so enable running on no tags as well

https://docs.gitlab.com/ee/ci/runners/configure_runners.html#set-a-runner-to-run-untagged-jobs

```yaml
build-job:
  tags:
    - awsec2
  script:
    - echo "Compiling the code..."
    - echo "Compile complete."
```

gitlab runner config file location - 
> /etc/gitlab-runner/config.toml

## GITLAB CI Templates

https://gitlab.com/gitlab-org/gitlab-foss/-/tree/master/lib/gitlab/ci/templates



