FROM python:3.9-slim-buster
RUN apt-get update && \
    apt-get install make
WORKDIR /app
COPY src/ src/
COPY makefile .
EXPOSE 5000
ENTRYPOINT [ "make", "run" ]